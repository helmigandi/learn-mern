# Learn MERN

- Progress: Video 12
- Time: 34:00

## Sources

- [Dave Gray playlist youtube](https://www.youtube.com/playlist?list=PL0Zuz27SZ-6P4dQUsoDatjEGpmBpcOW8V)
- [Setup Eslint and Prettier](https://blog.bitsrc.io/how-to-set-up-node-js-application-with-eslint-and-prettier-b1b7994db69f)
- [Dave Gray Github repository](https://github.com/gitdagray/mern_stack_course)
