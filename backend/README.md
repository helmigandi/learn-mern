# Learn MERN

Learn MERN Project from Dave Gray playlist youtube videos.

## Dependencies

1. Express;
2. Nodemon(dev);
3. Prettier(dev);
4. date-fns;
5. uuid;
6. cookie-parser;
7. cors;
8. mongoose;
9. env;
10. mongoose-sequence;
11. express-async-handler;
12. bcrypt;
13. express-rate-limit;
14. jsonwebtoken;

## MongoDB Config

- Cluster0;
- database name: `techNotesDB`;
- collection: `users`;
- username: `mongo-user`;
- password: `5qxrfuiSEgDlpSfo`;

## Create Random Access Token and Refresh Token

```bash
node

require('crypto').randomBytes(64).toString('hex')
```
